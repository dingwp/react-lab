import React, {Component} from 'react';

export default class Clock extends Component {
	constructor(props) {
		super(props);
		this.state = {date: new Date()}
	}

	componentDidMount() {
		// explain 'this' in arrow fn
		this.timerId = setInterval(() => {
			this.tick();
		}, 1000)

		// let self = this;
		// this.timerId = setInterval(function () {
		// 	// console.log(this)
		// 	self.tick()
		// }, 1000)
	}

	componentWillUnmount() {
		clearInterval(this.timerId);
	}

	tick() {
		this.setState({date: new Date()})
	}

	displayTime() {
		return this.state.date.toLocaleString('en-AU', {timeZone: this.props.timeZone});
	}

	handleClick(city) {
		alert(`${city} clicked !`);
		console.log(this.props.city);
	}

	render() {
		const {timeZone} = this.props;
		return (
			<div style={{margin:30, fontSize:28}}>
				<i className="fa fa-clock-o" />
				<span style={{paddingLeft:10}} >
					{`${timeZone} ${this.state.date.toLocaleString('en-Au', {timeZone})}`}
				</span>
			</div>
		)
	}
}

Clock.defaultProps = {
	timeZone: 'Australia/Brisbane'
}